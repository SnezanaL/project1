<?php
/*
Plugin name: My Metaboxes
Plugin URI: http://pcelinjaklalatovic.com
Description: Plugin to creation custom posts.
Version: 1.0
Author: Snezana Lalatovic
Autor URI: http://pcelinjaklalatovic.com
Text domain: tutsplus
License: GPLv2
*/

/***************************************
Metaoxes for diametar
***************************************/

// function to create metabox
function tutsplus_diameter_metabox () {
	
	add_meta_box(
		'tutsplus_diameter_metabox',
		__( 'Diameter of Moon', 'tutsplus' ),
		'tutsplus_diameter_metabox_callback',
		'moon',
		'normal',
		'high',
		''
	
	
	);
	
}
add_action ( 'add_meta_boxes', 'tutsplus_diameter_metabox' );

// metabox callback function
function tutsplus_diameter_metabox_callback () {
	
	// field and label
	$value = get_post_meta( $post->ID, 'Diameter (km)', true );
	echo '<label for="tutsplus_moon_diameter">';
	_e( 'Diameter of moon in km', 'tutsplus');
	echo '</label>';
	echo '<input type="number" id="tutsplus_moon_diameter" name="tutsplus_moon_diameter" value="' . esc_attr( $value ) . '" size="6" />';
	
}
?>