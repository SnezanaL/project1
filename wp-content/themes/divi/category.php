<?php get_header(); ?>

<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
			<h2 class="mons-headline">Category: <?php single_cat_title(); ?></h2>
		<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					$post_format = et_pb_post_format(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>

					<div class="mon-cat-content cat-box">
						<a href="<?php the_permalink() ?>">
							<?php the_post_thumbnail(); ?>
							<h2 class="entry-title job-post-title" itemprop="name headline">
								<?php the_title(); ?>
							</h2>
						</a>
			   
			  
			   
                <div class="cat-content"><p><?php the_excerpt();?></p></div>
				<div class="job-more-link"><a href="<?php the_permalink(); ?>" class="more-link">READ MORE</a></div>
               </div> <!-- .mon-cat-content .cat-box -->
				
					</article> <!-- .et_pb_post -->
			<?php
					endwhile;

					if ( function_exists( 'wp_pagenavi' ) )
						wp_pagenavi();
					else
						get_template_part( 'includes/navigation', 'index' );
				else :
					get_template_part( 'includes/no-results', 'index' );
				endif;
			?>
			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>