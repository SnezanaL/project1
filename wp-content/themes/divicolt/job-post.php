<?php /* Template Name: Template Job Post */
get_header();

 ?>

<div id="main-content">



  <div class="mons-blog-section et_pb_section  et_pb_section_0 et_pb_with_background et_section_regular" style="background-image: url(http://monsenso.basel-onlinemarketing.de/wp-content/uploads/2017/01/Careers-banner.jpg); min-height: 400px; background-position: center top;">
      <div class=" et_pb_row et_pb_row_0">
        <div class="et_pb_column et_pb_column_2_3  et_pb_column_0">
        <div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_left  et_pb_text_0">
          <h1 style="font-family:'Source Sans Pro', Helvetica, Arial, Lucida, sans-serif; font-weight: bold; font-size: 43px; letter-spacing: 2px;">Careers</h1>

          <p>&nbsp;</p>
          <p class="mons"><span style="color: #ffffff;">Join our team and help us leave the world better than we found it.</span></p>
          <p class="mons"><span style="color: #ffffff;">At Monsenso we don’t only create solutions – we are revolutionising the treatment of mental illness. We make a difference in people’s lives.<br>
          </span></p>
      </div> <!-- .et_pb_text -->
      </div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_1 et_pb_column_empty">
      </div> <!-- .et_pb_column -->
      </div> <!-- .et_pb_row -->
    </div><!-- .et_pb_section -->



<div class="event-content">
   <div class="container">
		     <div id="content-area" class="clearfix">

<div id="left-area">



  <?php if ( have_posts() ) : ?>

			<?php $query = new WP_Query( array('post_type' => 'job_post', 'posts_per_page' => 5 ) );
               while ( $query->have_posts() ) : $query->the_post(); ?>
               <div class="entry-content event-box">
                   <a href="<?php the_permalink() ?>">
                 <h2 class="entry-title job-post-title" itemprop="name headline">
                  <?php the_title(); ?>
                 </h2>
               </a>
                <div class="job-content"><p><?php the_excerpt();?></p></div>
<div class="job-more-link"><a href="<?php the_permalink(); ?>" class="more-link">READ MORE</a></div>
               </div>

			     <?php get_template_part( 'content', 'page' ); ?>

      <?php endwhile; ?>

    <?php endif; wp_reset_postdata(); ?>

			</div> <!-- #left-area -->

<div id="right-area">
			<?php //get_sidebar( 'event'); ?>
</div> <!-- #right-area -->
		</div> <!-- #content-area -->
	</div> <!-- .container -->

</div><!-- .event-content -->

</div> <!-- #main-content -->

<?php get_footer(); ?>
