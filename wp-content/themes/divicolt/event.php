<?php     /* Template Name: Template Event */
get_header();

     $is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); ?>

     <div id="main-content">

     <?php if ( ! $is_page_builder_used ) : ?>





               <div class="mons-blog-section et_pb_section  et_pb_section_0 et_pb_with_background et_section_regular" style="background-image: url(http://monsenso.basel-onlinemarketing.de/wp-content/uploads/2017/01/Blog-banner.jpg); min-height: 400px; background-position: center top;">
            <div class=" et_pb_row et_pb_row_0">
      				<div class="et_pb_column et_pb_column_2_3  et_pb_column_0">
      				<div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_left  et_pb_text_0">
                <h1>Events</h1>
                <p>&nbsp;</p>
                <p class="mons"><span style="color: #ffffff;">Learn more about Monsenso’s participation at international events</span></p>
                <p class="mons"><span style="color: #ffffff;">Monsenso sponsors several events throughout the year. We are committed to helping clinicians around the world provide a better treatment to more people at a lower cost.<br>
                </span></p>
      			</div> <!-- .et_pb_text -->
      			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_1 et_pb_column_empty">
      			</div> <!-- .et_pb_column -->
      			</div> <!-- .et_pb_row -->
          </div><!-- .et_pb_section -->

 <div class="container">          
   <div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

  <?php if ( have_posts() ) : ?>

			<?php $query = new WP_Query( array('post_type' => 'event', 'posts_per_page' => 5 ) );
               while ( $query->have_posts() ) : $query->the_post(); ?>
               <div class="entry-content">
                 <h2 class="entry-title" itemprop="name headline">
                     <?php the_title(); ?>
                 </h2>
                <?php the_content();?>
               </div>


			     <?php get_template_part( 'content', 'page' ); ?>

      <?php endwhile; ?>

    <?php endif; wp_reset_postdata(); ?>

			</div> <!-- #left-area -->

			<?php get_sidebar( 'event'); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->



</div> <!-- #main-content -->

<?php get_footer(); ?>
