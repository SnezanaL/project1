<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pcelinjak');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1+_7d:7Ykc*S.j,8!K^x5[lZEMv`41Cm:?7NP~@~UD8hl{*[lV.u2,CmizvN~;[O');
define('SECURE_AUTH_KEY',  '8qI;W_*?~8Zv!{sRXXn|O$x~AjzR_WQW&m/bPpiDpsc3R!%o3a/k|iMC1Oq21vj[');
define('LOGGED_IN_KEY',    'Pxdqs}>P0.p V{<KA@38 ^nWjO%e;Yj&KU !*LL+Ct$RI$0:po[J)(ef=)>C^rOK');
define('NONCE_KEY',        '4swzd:@~Gx;pr-`0*<u(tm@yKkak pOPt%J|NIY]Jg$a=x_Z}&x9=n#V<{5x{tGj');
define('AUTH_SALT',        '8 P&lVkhzP_6?b#nr26S~Vyf)*G]T;4A.>hK!!GBQ)2J|4bDYD(Sl(@Hx`c/)zJC');
define('SECURE_AUTH_SALT', '`,gw[Z@b26?hw.NV!yf]C-fRlF=;2[OG8>4aw7JH~7hvJ5]I+lB(S*tS9bQ8Zuls');
define('LOGGED_IN_SALT',   '38cp1?-TvqG~np3SLOZ@uVf>RKr}{|K&3tUleCOv`QqNeuHwXF{I#Zk/{c}9}f2?');
define('NONCE_SALT',       '26YyWK=+ZTupxyX#5,{bD*tzW*F#WO)@Rrs;o.A&&l$2K#j$(`n0_dY4~Xb0{c_,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
